# 1. Mampu menunjukkan keseluruhan Use Case beserta ranking dari tiap Use Case dari produk digital:

untuk paragraf yang di bold adalah usecase yang telah di koding

| No. | Use Case                           | Prioritas | Penjelasan                                                                                                                                           |
| --- | ---------------------------------- | --------- | ---------------------------------------------------------------------------------------------------------------------------------------------------- |
| 1   | `Cek saldo`     | Tinggi- 100   | Cek saldo dan mutasi rekening memiliki prioritas tinggi karena ini adalah fitur dasar dari sebuah aplikasi perbankan yang paling sering digunakan. |
| 2   | `Transfer`                      | Tinggi-100    | Transfer dana juga memiliki prioritas tinggi karena fitur ini memungkinkan nasabah untuk mentransfer dana dengan mudah dan cepat.                       |
| 3   | `Pembayaran tagihan`                 | Tinggi-100    | Pembayaran tagihan juga memiliki prioritas tinggi karena fitur ini memungkinkan nasabah untuk membayar tagihan dengan mudah dan menghindari keterlambatan pembayaran. |
| 4   | `Top up e-wallet dan tranfer e-wallet`          | Tinggi-100    | Top up e-wallet dan pulsa memiliki prioritas tinggi karena fitur ini memungkinkan nasabah untuk mengisi ulang saldo e-wallet dan membeli pulsa.        |
| 5   | Investasi                          | Sedang-70    | Investasi memiliki prioritas sedang karena fitur ini menawarkan layanan investasi seperti pembelian reksa dana dan obligasi.                            |
| 6   | Pemesanan tiket                    | Sedang-70    | Pemesanan tiket memiliki prioritas sedang karena fitur ini menyediakan kemudahan bagi nasabah untuk memesan tiket pesawat, kereta api, atau hotel.     |
| 7   | Aktivasi kartu debit/kredit        | Rendah-40    | Aktivasi kartu debit/kredit memiliki prioritas rendah karena fitur ini jarang digunakan oleh nasabah dan bukan fitur utama aplikasi perbankan.           |
| 8   | Pembukaan rekening baru            | Rendah-50    | Pembukaan rekening baru memiliki prioritas rendah karena proses ini hanya tersedia untuk nasabah yang sudah memiliki rekening di bank.                 |
| 9   | Permintaan cek/buku tabungan       | Rendah-40    | Permintaan cek/buku tabungan memiliki prioritas rendah karena fitur ini tidak sering digunakan oleh nasabah.                                          |
| 10  | Permintaan kartu ATM/debit baru    | Rendah-50    | Permintaan kartu ATM/debit baru memiliki prioritas rendah karena fitur ini jarang digunakan oleh nasabah.                                             |
| 11  | Pembayaran angsuran kredit         | Rendah-30    | Pembayaran angsuran kredit memiliki prioritas rendah karena fitur ini hanya relevan bagi nasabah yang memiliki kredit.                                |
| 12  | Pengaturan notifikasi dan pemberitahuan | Sedang-70 | Pengaturan notifikasi dan pemberitahuan memiliki prioritas sedang karena fitur ini memungkinkan nasabah mengelola pengaturan pemberitahuan.            |
| 13  | Penggantian PIN                    | Rendah-50    | Penggantian PIN memiliki prioritas rendah karena fitur ini jarang digunakan oleh nasabah.                                                             |
| 14  | Pembayaran online                  | Sedang-70    | Pembayaran online memiliki prioritas sedang karena fitur ini memungkinkan nasabah untuk melakukan pembayaran secara online.                            |
| 15  | Penarikan tunai di ATM             | Rendah-50    | Penarikan tunai di ATM memiliki prioritas rendah karena fitur ini umumnya dapat dilakukan di mesin ATM langsung.                                      |
| 16  | Pengiriman uang                    | Rendah-50    | Pengiriman uang memiliki prioritas rendah karena fitur ini jarang digunakan oleh nasabah.                                                             |
| 17  | Pembukaan deposito                 | Rendah-50    | Pembukaan deposito memiliki prioritas rendah karena fitur ini tidak sering digunakan oleh nasabah.                                                    |
| 18  | Penjadwalan transfer                | Rendah-30    | Penjadwalan transfer memiliki prioritas rendah karena fitur ini tidak sering digunakan oleh nasabah.                                                   |
| 19  | Penukaran mata uang                | Rendah-30    | Penukaran mata uang memiliki prioritas rendah karena fitur ini jarang digunakan oleh nasabah.                                                          |
| 20  | Pembelian asuransi                 | Rendah-30    | Pembelian asuransi memiliki prioritas rendah karena fitur ini tidak sering digunakan oleh nasabah.         



# 2. Mampu mendemonstrasikan Class Diagram dari keseluruhan Use Case produk digital

Class Diagram menggunakan Mermaid

```mermaid
classDiagram
    class BCA_Mobile {
        +nomorRekening: string
        +saldo: float
        +namaNasabah: string
        +transfer(): void
        +isiSaldo(): void
        +bayarTagihan(): void
        +logout(): void
    }

    class KonfirmasiTransfer {
        +nomorRekeningTujuan: string
        +jumlahTransfer: float
        +sisaSaldo: float
    }

    class CekSaldo {
        +nomorRekening: string
        +saldo: float
        +namaNasabah: string
    }

    class Transfer {
        +nomorRekeningTujuan: string
        +jumlahTransfer: float
    }

    class TransferOVO {
        +nomorRekeningTujuan: string
        +jumlahTransfer: float
    }

    class TransferDANA {
        +nomorRekeningTujuan: string
        +jumlahTransfer: float
    }

    class IsiSaldo {
        +jumlahIsiSaldo: float
    }

    class BayarTagihan {
        +namaTagihan: string
    }

    class Logout {
        +logout(): void
    }

    BCA_Mobile --> CekSaldo
    BCA_Mobile --> Transfer
    BCA_Mobile --> IsiSaldo
    BCA_Mobile --> BayarTagihan
    BCA_Mobile --> Logout
    Transfer --> KonfirmasiTransfer
    TransferOVO --> Transfer
    TransferDANA --> Transfer
```


# 3. Mampu menunjukkan dan menjelaskan penerapan setiap poin dari SOLID Design Principle

**Single Responsibility Principle (SRP)**
 diterapkan pada masing-masing kelas controller, yaitu CekSaldoController, TransferController, EWalletController, IsiSaldoController, TagihanController, RiwayatTransferController, dan LogoutController.

Setiap kelas controller bertanggung jawab atas satu tugas spesifik:

- CekSaldoController bertanggung jawab untuk menampilkan saldo.
- TransferController bertanggung jawab untuk melakukan transfer.
- EWalletController bertanggung jawab untuk menampilkan informasi e-wallet.
- IsiSaldoController bertanggung jawab untuk mengisi saldo.
- TagihanController bertanggung jawab untuk membayar tagihan.
- RiwayatTransferController bertanggung jawab untuk menampilkan riwayat transfer.
- LogoutController bertanggung jawab untuk melakukan proses logout.

```
// CekSaldoController.php
class CekSaldoController {
    private $saldoService;

    public function __construct(SaldoServiceInterface $saldoService) {
        $this->saldoService = $saldoService;
    }

    public function cekSaldo() {
        // Logika untuk cek saldo
        $saldo = $this->saldoService->getSaldo();

        // Tampilkan saldo ke halaman cek_saldo.php
        include 'cek_saldo.php';
    }
}

// TransferController.php
class TransferController {
    private $transferService;

    public function __construct(TransferServiceInterface $transferService) {
        $this->transferService = $transferService;
    }

    public function transfer() {
        // Logika untuk transfer
        $transferResult = $this->transferService->transfer();

        // Tampilkan hasil transfer ke halaman konfirmasi_transfer.php
        include 'konfirmasi_transfer.php';
    }
}

// EWalletController.php
class EWalletController {
    private $eWalletService;

    public function __construct(EWalletServiceInterface $eWalletService) {
        $this->eWalletService = $eWalletService;
    }

    public function eWallet() {
        // Logika untuk e-wallet
        $eWalletResult = $this->eWalletService->eWallet();

        // Tampilkan hasil e-wallet ke halaman e_wallet.php
        include 'e_wallet.php';
    }
}

// IsiSaldoController.php
class IsiSaldoController {
    private $isiSaldoService;

    public function __construct(IsiSaldoServiceInterface $isiSaldoService) {
        $this->isiSaldoService = $isiSaldoService;
    }

    public function isiSaldo() {
        // Logika untuk isi saldo
        $isiSaldoResult = $this->isiSaldoService->isiSaldo();

        // Tampilkan hasil isi saldo ke halaman konfirmasi_isi_saldo.php
        include 'konfirmasi_isi_saldo.php';
    }
}

// TagihanController.php
class TagihanController {
    private $tagihanService;

    public function __construct(TagihanServiceInterface $tagihanService) {
        $this->tagihanService = $tagihanService;
    }

    public function bayarTagihan() {
        // Logika untuk bayar tagihan
        $bayarTagihanResult = $this->tagihanService->bayarTagihan();

        // Tampilkan hasil bayar tagihan ke halaman konfirmasi_pembayaran.php
        include 'konfirmasi_pembayaran.php';
    }
}

// RiwayatTransferController.php
class RiwayatTransferController {
    private $riwayatTransferService;

    public function __construct(RiwayatTransferServiceInterface $riwayatTransferService) {
        $this->riwayatTransferService = $riwayatTransferService;
    }

    public function tampilkanRiwayatTransfer() {
        // Logika untuk tampilkan riwayat transfer
        $riwayatTransfer = $this->riwayatTransferService->getRiwayatTransfer();

        // Tampilkan riwayat transfer ke halaman riwayat_transfer.php
        include 'riwayat_transfer.php';
    }
}

// LogoutController.php
class LogoutController {
    public function logout() {
        // Logika untuk logout
        // ...
    }
}
```

**Open-Closed Principle (OCP)** 

```
// SaldoServiceInterface.php
interface SaldoServiceInterface {
    public function getSaldo();
}

// TransferServiceInterface.php
interface TransferServiceInterface {
    public function transfer();
}

// EWalletServiceInterface.php
interface EWalletServiceInterface {
    public function eWallet();
}

// IsiSaldoServiceInterface.php
interface IsiSaldoServiceInterface {
    public function isiSaldo();
}

// TagihanServiceInterface.php
interface TagihanServiceInterface {
    public function bayarTagihan();
}

// RiwayatTransferServiceInterface.php
interface RiwayatTransferServiceInterface {
    public function getRiwayatTransfer();
}
```

**Interface Segregation Principle (ISP)**

 interface SaldoServiceInterface hanya memiliki satu metode getSaldo(), yang merupakan fungsi untuk mendapatkan saldo. Begitu pula dengan interface-interface lainnya seperti TransferServiceInterface, EWalletServiceInterface, dan lain-lain, semuanya memiliki metode-metode yang relevan dengan fungsionalitas masing-masing.


```
// SaldoServiceInterface.php
interface SaldoServiceInterface {
    public function getSaldo();
}

// TransferServiceInterface.php
interface TransferServiceInterface {
    public function transfer();
}

// EWalletServiceInterface.php
interface EWalletServiceInterface {
    public function eWallet();
}

// IsiSaldoServiceInterface.php
interface IsiSaldoServiceInterface {
    public function isiSaldo();
}

// TagihanServiceInterface.php
interface TagihanServiceInterface {
    public function bayarTagihan();
}

// RiwayatTransferServiceInterface.php
interface RiwayatTransferServiceInterface {
    public function getRiwayatTransfer();
}
```


# 4. Mampu menunjukkan dan menjelaskan Design Pattern yang dipilih

**Singleton**

Singleton pattern adalah pola desain yang digunakan untuk memastikan hanya ada satu instance dari sebuah kelas yang dapat dibuat dan diakses. Dalam implementasinya, kelas Singleton memiliki konstruktor yang private dan menyediakan metode statis untuk mengakses instance Singleton. Dengan Singleton pattern, kita dapat memastikan bahwa hanya ada satu instance dari kelas yang dapat diakses secara global. Namun, penggunaannya perlu diperhatikan dengan cermat untuk menghindari ketergantungan berlebihan dan mempertimbangkan masalah konkurensi jika diperlukan.

```
<?php
class Singleton {
    private static $instance;
    
    // Private constructor untuk mencegah pembuatan instance dari luar kelas
    private function __construct() {
        // Inisialisasi objek singleton
    }

    // Static method untuk mendapatkan instance Singleton
    public static function getInstance() {
        if (!self::$instance) {
            self::$instance = new self();
        }
        return self::$instance;
    }
    
    // Metode lainnya
    public function someMethod() {
        // Logika metode lainnya
    }
}

$singleton = Singleton::getInstance();
$singleton->someMethod();
?>
```


# 5. Mampu menunjukkan dan menjelaskan konektivitas ke database

![ss](ss/no5.png)

Kode di atas melakukan otentikasi pengguna dengan memeriksa username dan password yang diberikan melalui formulir login dengan data yang ada di tabel "user" dalam database.

Dengan menambahkan langkah-langkah di atas,akan memiliki koneksi database yang aktif yang dapat digunakan untuk menjalankan kueri otentikasi dan memeriksa apakah pengguna berhasil masuk atau tidak.


# 6. Mampu menunjukkan dan menjelaskan pembuatan web service dan setiap operasi CRUD nya

- **Web service yang dipakai**

Slim FrameWork V3

- **Web Page Untuk User**

![ss](ss/hal1.png)

- **Web Service Untuk Aplikasi**

![ss](ss/no5.png)

- **CRUD**

CRUD disini berfungsi untuk membuat transfer,memlihat saldo,meng-update saldo,dan mendelete riwayat transfer.
- create

user dapat membuat tranfer dengan cara klik tranfer lalu akan di proses di transfer.php

```
<?php
if ($_SERVER["REQUEST_METHOD"] == "POST") {
    // Mendapatkan nilai yang dikirimkan melalui metode POST
    $nomorRekeningTujuan = $_POST["nomorRekeningTujuan"];
    $jumlahTransfer = $_POST["jumlahTransfer"];

    // Lakukan proses transfer di sini
    $konfirmasiTransfer = array(
        "nomorRekeningTujuan" => $nomorRekeningTujuan,
        "jumlahTransfer" => $jumlahTransfer,
        "sisaSaldo" => $saldo - $jumlahTransfer
    );
    
    // Redirect ke halaman konfirmasi_transfer.php dengan mengirimkan data melalui query parameter
    header("Location: konfirmasi_transfer.php?" . http_build_query($konfirmasiTransfer));
    exit;

    // Setelah proses transfer selesai, Anda dapat melakukan redirect ke halaman konfirmasi atau halaman lainnya
    
}
?>

<!DOCTYPE html>
<html>
<head>
    <title>BCA Mobile - Transfer</title>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0/dist/css/bootstrap.min.css">
</head>
<body>
    <div class="container">
        <h1>Transfer</h1>
        <hr>
        <form action="<?php echo $_SERVER["PHP_SELF"]; ?>" method="post">
            <div class="mb-3">
                <label for="nomorRekeningTujuan" class="form-label">Nomor Rekening Tujuan</label>
                <input type="text" class="form-control" id="nomorRekeningTujuan" name="nomorRekeningTujuan">
            </div>
            <div class="mb-3">
                <label for="jumlahTransfer" class="form-label">Jumlah Transfer</label>
                <input type="text" class="form-control" id="jumlahTransfer" name="jumlahTransfer">
            </div>
            <button type="submit" class="btn btn-primary">Transfer</button>
        </form>
        <hr>
        <a href="bca_mobile.php" class="btn btn-primary">Kembali ke Menu Utama</a>
    </div>
</body>
</html>
```
![ss](ss/crt.png)

- Read

User dapat melihat saldo yang ada dari web di file cek_saldo.php

```
<?php
// Simulasikan data saldo dari database atau sumber data lainnya
$nomorRekening = "123456789";
$saldo = 1000000;
$namaNasabah = "Diaz";

// Anda juga dapat mengganti kode di atas dengan proses yang sesuai untuk mendapatkan data saldo

?>

<!DOCTYPE html>
<html>
<head>
    <title>BCA Mobile - Cek Saldo</title>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0/dist/css/bootstrap.min.css">
</head>
<body>
    <div class="container">
        <h1>Cek Saldo</h1>
        <hr>
        <h3>Saldo <?php echo $namaNasabah; ?></h3>
        <p>Nomor Rekening: <?php echo $nomorRekening; ?></p>
        <p>Saldo Anda adalah: Rp <?php echo number_format($saldo, 2, ',', '.'); ?></p>
        <hr>
        <a href="bca_mobile.php" class="btn btn-primary">Kembali ke Menu Utama</a>
    </div>
</body>
</html>
```
![ss](ss/rd.png)

- Update

User dapat mambahkan saldo dengan isi_saldo.php

```
<?php
if ($_SERVER["REQUEST_METHOD"] == "POST") {
    // Mendapatkan nilai yang dikirimkan melalui metode POST
    $jumlahIsiSaldo = $_POST["jumlahIsiSaldo"];

    // Lakukan proses pengisian saldo di sini
    // ...

    // Setelah proses pengisian saldo selesai, Anda dapat melakukan redirect ke halaman konfirmasi atau halaman lainnya
    header("Location: konfirmasi_isi_saldo.php?jumlahIsiSaldo=" . $jumlahIsiSaldo);
    exit;
}
?>

<!DOCTYPE html>
<html>
<head>
    <title>BCA Mobile - Isi Saldo</title>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0/dist/css/bootstrap.min.css">
</head>
<body>
    <div class="container">
        <h1>Isi Saldo</h1>
        <hr>
        <form action="<?php echo $_SERVER["PHP_SELF"]; ?>" method="post">
            <div class="mb-3">
                <label for="jumlahIsiSaldo" class="form-label">Jumlah Isi Saldo</label>
                <input type="text" class="form-control" id="jumlahIsiSaldo" name="jumlahIsiSaldo">
            </div>
            <button type="submit" class="btn btn-primary">Isi Saldo</button>
        </form>
        <hr>
        <a href="bca_mobile.php" class="btn btn-primary">Kembali ke Menu Utama</a>
    </div>
</body>
</html>
```
![ss](ss/is.png)

- Delete

User dapat mendelete riwayat transfer jika merasa tidak dibutuhkan lagi dan akan diproses di proses_hapus.php

```
<?php
// Simulasikan riwayat transfer dari database atau sumber data lainnya
$riwayatTransfer = array(
    "Transfer sebesar 500000 ke nomor rekening 123456789",
    "Transfer sebesar 200000 ke nomor rekening 987654321",
    "Transfer sebesar 100000 ke nomor rekening 456789123"
);

// Fungsi untuk menghapus riwayat transfer berdasarkan indeks
function hapusRiwayatTransfer($index)
{
    global $riwayatTransfer;
    if (isset($riwayatTransfer[$index])) {
        unset($riwayatTransfer[$index]);
    }
}

// Periksa apakah indeks riwayat transfer disediakan sebagai parameter
if (isset($_GET['index'])) {
    $index = $_GET['index'];

    // Panggil fungsi hapusRiwayatTransfer untuk menghapus riwayat transfer
    hapusRiwayatTransfer($index);

    // Redirect ke halaman riwayat_transfer.php setelah berhasil menghapus
    header("Location: riwayat_transfer.php");
    exit;
} else {
    // Jika indeks tidak tersedia, redirect ke halaman riwayat_transfer.php
    header("Location: riwayat_transfer.php");
    exit;
}
```
![ss](ss/dlt.png)


# 7. Mampu menunjukkan dan menjelaskan Graphical User Interface dari produk digital

- Menu Navigasi

Terdapat menu navigasi di halaman utama yang memungkinkan user / pengguna untuk beralih ke fitur yang berbeda.

- Konten Utama

Bagian ini merupakan area halaman utama yang menampilkan informasi atau fungsi utama produk, seperti fitur Cek Saldo, Transfer,E-Wallet, Isi Saldo, Tagihan,Tampilkan Riwayat Transfer, dan Logout

- Header

Header berisi judul produk yang mengidentifikasikan halaman utama atau fitur yang sedang digunakan.

![ss](ss/hal1.png)

# 8. Mampu menunjukkan dan menjelaskan HTTP connection melalui GUI produk digital

- URL Input:Memasukkan URL tujuan ke dalam bidang input ini. URL ini mewakili alamat tujuan untuk melakukan koneksi HTTP,"http://localhost/PBO%20BCA%20PHP/index.php".

- HTTP Methods: Terdapat pilihan metode HTTP yang tersedia, seperti GET, POST.


- Headers: Pengguna dapat menambahkan header tambahan yang akan dikirimkan dalam koneksi HTTP. Header ini berisi informasi tambahan yang dapat digunakan oleh server untuk memproses permintaan dengan benar.

# 9. Mampu Mendemonstrsikan produk digitalnya kepada publik dengan cara-cara kreatif melalui video Youtube

